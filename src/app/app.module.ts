import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CallHttpService } from './services/call-http.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { PersistenceService } from 'angular-persistence';
import { LoginAuthService } from '../app/login-auth.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [CallHttpService,PersistenceService,LoginAuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
