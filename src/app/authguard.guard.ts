import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PersistenceService, StorageType } from 'angular-persistence';
import { LoginAuthService } from './login-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate {
  getLoginFlag : boolean;
 
  constructor(private routes : Router,
    private authService : LoginAuthService,
    public loginFlagPersistenceService: PersistenceService,){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean  {
      let url: string = state.url;
      return this.checkLogin(url);
  }
  checkLogin(url: string): boolean {
    this.getLoginFlag = this.loginFlagPersistenceService.get(
      "loginFlag",
      StorageType.SESSION
    );
    if (this.authService.getLoginFlag) { return true; }

    // Store the attempted URL for redirecting
    this.authService.redirectUrl = url;

    // Navigate to the login page with extras
    this.routes.navigate(['/login']);
    return false;
  }

}
