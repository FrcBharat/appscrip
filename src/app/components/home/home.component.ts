import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { PersistenceService, StorageType } from 'angular-persistence';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  homeForm: FormGroup;
  submitted = false;

  showEmail = [];

  constructor(private formBuilder: FormBuilder,
    private router:Router,
    private loginpersistenceService : PersistenceService) { }

  ngOnInit() {
    this.homeForm = this.formBuilder.group({
    
      email: ['', [Validators.required, Validators.email]],
     
  });

  }
  get f() { return this.homeForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.homeForm.invalid) {
        return;
    }
    console.log(JSON.stringify(this.homeForm.value));
    
    console.log(this.homeForm.value);
    var exampleArray = []
    this.showEmail.push({'Email' : this.homeForm.value.email})
   // this.showEmail.push(this.homeForm.value);  
   console.log('added successfully');
   
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value))
}
clear_cache() {
    console.log("clearing cache");
    
  // clear cache variables;
  this.loginpersistenceService.remove('loginFlag',StorageType.SESSION);

  // clears all storage saved by this service, and returns a list of keys that were removed;
  this.loginpersistenceService.removeAll(StorageType.SESSION);    
  
  //cleans the storage of expired objects
  this.loginpersistenceService.clean(StorageType.SESSION);  
  this.router.navigate(['/login']);
}

}
