import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { CallHttpService } from '../../services/call-http.service';
import { PersistenceService ,StorageType} from 'angular-persistence';
import { LoginAuthService } from '../../login-auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;

 data: any;
 email: AbstractControl;
 token: AbstractControl;

  constructor(private formBuilder: FormBuilder,
    private router:Router,
    private callhttp: CallHttpService,
    private authService : LoginAuthService,
    private loginFlagPersistenceService : PersistenceService) { }

 

  ngOnInit() {

        // Controls are used in me-login.component.html for accessing values and checking functionalities
       

    this.loginForm = this.formBuilder.group({
   
      email: ['', [Validators.required, Validators.email]],
      token: ['', [Validators.required, Validators.minLength(6)]]
  });
  this.email = this.loginForm.controls["email"];
  this.token = this.loginForm.controls["token"];

  }
  get f() { return this.loginForm.controls; }


  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }


this.data = this.loginForm.value;
    console.log(this.loginForm.value);
console.log("values",this.data);


//     this.callhttp.onlogin(this.data).subscribe(response => {
// console.log(response);

//     });
    
this.callhttp.onlogin(this.data).subscribe(resp=>{
  console.log(resp);
  console.log(resp.message);
  
  if(resp.message == 'verified user..!!'){

    this.loginFlagPersistenceService.set("loginFlag", true,{type: StorageType.SESSION});
    console.log("successful");

    this.authService.login().subscribe(() => {
      console.log(this.authService.isLoggedIn);
      if (this.authService.isLoggedIn) {
        
        console.log("navigate");
           // if the login is valid navigate to home page
      //  this.router.navigate(["home"]);
      this.router.navigate(['/home']);
       // this.router.navigateByUrl("/home");
     
      }
 
    });
    
 
    
  } else {
    this.loginFlagPersistenceService.set("loginFlag", false,{type: StorageType.SESSION});
    
    console.log("unsuccessful");
    
  }


});

   
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value))
}


  }

