import { Injectable } from '@angular/core';
import { PersistenceService, StorageType } from 'angular-persistence';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginAuthService {
  getLoginFlag = false;
  isLoggedIn = false;
  redirectUrl: string;

  constructor( public loginFlagPersistenceService: PersistenceService) {

    this.getLoginFlag = this.loginFlagPersistenceService.get(
      "loginFlag",StorageType.SESSION);
    if(this.getLoginFlag === undefined)
    {
      this.getLoginFlag = false;
    }
   }

   login(): Observable<boolean> {
    console.log(this.isLoggedIn);
    console.log( this.getLoginFlag , 'this.getLoginFlag');
    this.getLoginFlag=true;
    this.loginFlagPersistenceService.set("loginFlag", this.getLoginFlag,{type: StorageType.SESSION});
    return Observable.of(true).delay(1000).do(val => this.isLoggedIn = true);
  }
}
