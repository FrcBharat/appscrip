import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AuthguardGuard } from '../app/authguard.guard';


const routes: Routes = [


  { path: '',redirectTo: '/login', pathMatch: 'full'
  },
  { path: 'login', component: LoginComponent },
  {path: 'home', canActivate: [AuthguardGuard],component: HomeComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
